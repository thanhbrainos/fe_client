import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SearchWrapper, InputText, ImageClose } from '../../containers/debugLogs/showDebugLogsStyle';
import { images } from '../../theme';

class SearchLog extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const {
      handleSearch, valueInput, clearValue, clearInputStatus,
    } = this.props;
    return (
      <SearchWrapper>
        <InputText
          value={valueInput}
          onChange={event => handleSearch(event.target.value)}
        />
        {
          clearInputStatus && (
          <ImageClose
            src={images.offStatus}
            onClick={() => clearValue()}
          />
          )
        }
      </SearchWrapper>
    );
  }
}

SearchLog.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  clearValue: PropTypes.func.isRequired,
  clearInputStatus: PropTypes.any.isRequired,
  valueInput: PropTypes.any.isRequired,

};

export default SearchLog;
