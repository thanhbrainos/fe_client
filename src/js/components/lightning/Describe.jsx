import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { images } from '../../theme';
import LightningBody from './LightningBody';
// import i18n from '../../i18n/i18n';

import noticeImage from '../../../assets/imgs/noticeFGC.png';

const WrapperDescribe = styled.div`
  width: 100%;
  // position: absolute;
  top: 55%;
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family:
    "SF Pro JP",
    "SF Pro Display",
    "SF Pro Icons",
    "Hiragino Kaku Gothic Pro",
    "ヒラギノ角ゴ Pro W3",
    "メイリオ",
    "Meiryo",
    "ＭＳ Ｐゴシック",
    "Helvetica Neue",
    "Helvetica",
    "Arial",
    sans-serif;
`;

const ContentDescribe = styled.div`
  margin-top: 1.2em;
  width: 90%;
`;

const ElementDescribe = styled.div`
  margin-top: 3em;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const ImgDescribe = styled.img`
  width: 100%;
  max-width: 1200px;
  align-self: center;
`;

const ImgDescribeContent = styled.img`
  width: 100%;
  border-radius: 1.2em;
  max-width: 1080px;
`;

// const NoticeWrapper = styled.div`
//   color: #fff;
//   width: 80%;
//   background-color: #070707;
//   border: 5px solid #737474; 
//   border-radius: 10px;
//   padding: 2em;
//   margin-bottom: 30px;
// `;

// const NoticeTitle = styled.div`
//   text-align: center;
//   font-size: 2em;
//   font-weight: bold;
//   margin-bottom: 30px;
// `;

// const NoticeContent = styled.div`
//   text-align: center;
//   white-space: pre-wrap;
//   font-size: 1.5em;
// `;

// const HighLight = styled.span`
//   color: red;
// `;

class Describe extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const lengthContent = Object.keys(images.describe).length / 2;
    const contentDescribe = [];
    for (let i = 1; i < lengthContent + 1; i += 1) {
      const contentFieldName = 'describe'.concat(i);
      const titleFieldName = 'describeTitle'.concat(i);

      contentDescribe.push((
        <ElementDescribe key={i}>
          <ImgDescribe src={images.describe[titleFieldName]} />
          <ContentDescribe>
            <ImgDescribeContent src={images.describe[contentFieldName]} />
          </ContentDescribe>
        </ElementDescribe>
      ));
    }

    return (
      <WrapperDescribe>
        {/* <NoticeWrapper>
          <NoticeTitle>
            {i18n.t('noticeFGC.title')}
          </NoticeTitle>
          <NoticeContent>
            {i18n.t('noticeFGC.content1')}
            <HighLight>
              {i18n.t('noticeFGC.content2')}
            </HighLight>
            {i18n.t('noticeFGC.content3')}
            <HighLight>
              {i18n.t('noticeFGC.content4')}
            </HighLight>
            {i18n.t('noticeFGC.content5')}
          </NoticeContent>
        </NoticeWrapper> */}
        <ImgDescribe src={noticeImage} style={{ maxWidth: 1080 }} />
        {contentDescribe}
        <LightningBody
          locationBottom
          onChangePageType={this.props.onChangePageType}
        />
        <ImgDescribe src={images.noteRed} width={100} />
      </WrapperDescribe>
    );
  }
}

Describe.propTypes = {
  onChangePageType: PropTypes.func.isRequired,
};

export default Describe;
