import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
// import { disableScroll, enableScroll } from '../../helper/ControlScrollEvent';

export const getScaleForPopup = (defaultHeight = 650) => {
  const clientHeight = window.innerHeight;
  const clientWidth = window.innerWidth;
  if (clientWidth < 600) return 0.8;
  return (clientWidth / 1060 > clientHeight / defaultHeight)
    ? clientHeight / defaultHeight : clientWidth / 1060;
};

const DarkBackground = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 999999;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transform: translate3d(0, 0, 0);

  @media (max-width: 991px) {
    &.un-scale {
      > div:last-child {
        transform: scale(1);
      }
    }
  }
`;

const defaultState = {
  isShowPopupManager: false,
  customPopUp: null,
  clickOutsizeEvent: null,
};

const StyledBackground = styled.div`
  background-color: rgba(0, 0, 0, 0.6);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const DivScale = styled.div`
  transform: scale(${props => props.scale}, ${props => props.scale});
`;

const preventClickCross = (event) => {
  event.stopPropagation();
  event.nativeEvent.stopImmediatePropagation();
  event.preventDefault();
};

class PopupManager extends Component {
  constructor(props) {
    super(props);
    const { defaultHeight } = this.props;
    const scaleContent = getScaleForPopup(defaultHeight);
    this.state = {
      ...defaultState,
      scaleContent,
      isPreventScale: false,
    };
    PopupManager.instance = this;
    this.callBackOnClose = null;
    this.onResize = this.onResize.bind(this);
    this.onClickOutSize = this.onClickOutSize.bind(this);
    window.addEventListener('resize', this.onResize);
  }

  componentWillUnmount() {
    if (PopupManager.instance === this) PopupManager.instance = null;
    window.removeEventListener('resize', this.onResize);
  }

  onResize() {
    const { isPreventScale } = this.state;
    if (isPreventScale) return;
    const { defaultHeight } = this.props;
    const scaleContent = getScaleForPopup(defaultHeight);
    this.setState({ scaleContent });
  }

  onClickOutSize() {
    const { clickOutsizeEvent } = this.state;
    if (clickOutsizeEvent) clickOutsizeEvent();
  }

  closePopupManager() {
    this.setState(defaultState);
    document.body.style.overflow = 'initial';
    document.body.style.height = 'auto';
  }

  showCustomPopup(
    childComponent,
    clickOutsizeEvent = null,
    isPreventScale = false,
  ) {
    const { defaultHeight } = this.props;
    this.setState({
      isShowPopupManager: true,
      customPopUp: childComponent,
      clickOutsizeEvent,
      scaleContent: isPreventScale ? null : getScaleForPopup(defaultHeight),
      isPreventScale,
    });
  }

  render() {
    const {
      isShowPopupManager,
      scaleContent,
      customPopUp,
    } = this.state;
    if (isShowPopupManager === false) return (<div id="PopupManager" />);
    return (
      <DarkBackground id="PopupManager" onClick={this.onClickOutSize}>
        <StyledBackground />
        <DivScale scale={scaleContent} onClick={preventClickCross}>{customPopUp}</DivScale>
      </DarkBackground>
    );
  }
}

PopupManager.defaultProps = {
  defaultHeight: 650,
};

PopupManager.propTypes = {
  defaultHeight: PropTypes.number,
};

export default PopupManager;
