import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import {
  MainContainer,
  Title,
  UL,
  ChipIcon,
  TextItemContent,
  StyledTextHeaderList,
  TableWrap,
  TableContainer,
  StyledHeaderList,
  StyledLineRight,
} from './GroupGameChipStyle';

import StyleNumber from '../../StyleNumber';
import images from '../../../theme/images';
import i18n from '../../../i18n/i18n';
import lucImage from '../../../../assets/lucImage';

export const TextItem = styled.div`
  height: 100%;
  color: white;
  display: ${props => (props.display ? props.display : 'block')};
  flex: ${props => (props.flex ? props.flex : null)};
  justify-content: ${props => (props.justifyContent ? props.justifyContent : null)};
  align-items: center;
  text-align: ${props => (props.textAlign ? props.textAlign : 'left')};
  width: ${props => props.width};
  padding: ${props => (props.padding ? props.padding : 'unset')};
  line-height: 15px;
  font-size: 12px;
  white-space: nowrap;
  user-select: text;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    width: 1px;
    height: 100%;
    top: 0;
    right: 0;
    background-color: #666;
  }

  &#ip {
    word-break: break-word;
    white-space: pre-wrap;
  }

  span {
    padding-left: 5px;
    white-space: pre-line;
  }
`;

export const StyledLoadingWrap = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const StyledTextItem = styled(TextItem)`
  display: flex;
  width: ${props => props.widthPC || '250px'};
  justify-content: ${(props) => {
    if (props.alignLeft) return 'flex-end';
    if (props.alignRight) return 'flex-start';
    return 'center';
  }}
  @media only screen and (max-width: 480px) {
    font-size: 3.5vw;
    width: ${props => props.widthMB || '42vw'};
  }
`;

export const Li = styled.li`
  flex: 1;
  min-height: 28px;
  display: flex;
  align-items: center;
  margin-left: -40px;
  border-bottom: 0.5px solid #666;
  background: ${props => props.color};
  ${props => props.hasDetails && 'cursor: pointer'}
`;

export const CloseImageWrapper = styled.div`
  position: absolute;
  padding: 15px;
  top: 0;
  right: 0;
  cursor: pointer;
`;

export const CloseImage = styled.img`
  height: 15px;
  width: 15px;
`;

const DarkBackground = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  position: fixed;
  z-index: 1060;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transform: translate3d(0, 0, 0);
  background-color: rgba(10,10,10,0.29);

  @media (max-width: 991px) {
    &.un-scale {
      > div:last-child {
        transform: scale(1);
      }
    }
  }
`;

class GroupGameChipPopup extends Component {
  renderListChipCategories() {
    const { groupChipDetail } = this.props;
    const keys = Object.keys(groupChipDetail);

    return (
      <TableContainer>
        <TableWrap height="200px">
          <React.Fragment>
            <UL>
              <StyledHeaderList>
                <StyledTextHeaderList>
                  {i18n.t('tgc&fgc')}
                </StyledTextHeaderList>
                <StyledTextHeaderList fontSizeMb="3vw">
                  {i18n.t('groupChip.value')}
                </StyledTextHeaderList>
              </StyledHeaderList>
              {keys.map(chip => (
                <Li key={chip}>
                  <StyledTextItem
                    alignLeft
                  >
                    <TextItemContent alignLeft>
                      <ChipIcon src={lucImage.coins.FGCchip} alt="Chip icon" />
                      {chip}
                    </TextItemContent>
                  </StyledTextItem>
                  <StyledTextItem
                    alignRight
                  >
                    <TextItemContent
                      alignRight
                      widthPC="66%"
                    >
                      <StyleNumber value={groupChipDetail[chip]} />
                    </TextItemContent>
                  </StyledTextItem>
                </Li>
              ))}
            </UL>
          </React.Fragment>
        </TableWrap>
      </TableContainer>
    );
  }

  render() {
    const {
      handleCloseGroupChipDetail,
    } = this.props;
    return (
      <MainContainer id="GroupGameChipPopup">
        <Title>{i18n.t('groupChip.title')}</Title>
        <CloseImageWrapper onClick={handleCloseGroupChipDetail}>
          <CloseImage src={images.iconX} />
        </CloseImageWrapper>
        {this.renderListChipCategories()}
      </MainContainer>
    );
  }
}

GroupGameChipPopup.propTypes = {
  handleCloseGroupChipDetail: PropTypes.func.isRequired,
  groupChipDetail: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default GroupGameChipPopup;
