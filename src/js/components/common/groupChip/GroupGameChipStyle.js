import styled from 'styled-components';
import LoadingAnimation from '../LoadingVideo';

export const LineRight = styled.div`
  height: ${props => (props.height ? props.height : '412px')};
  width: 1px;
  background: #888;
  position: absolute;
  margin-top: 0;
  margin-left: ${props => props.left};
`;

export const TextHeaderList = styled.div`
  color: #000;
  display: ${props => (props.display ? props.display : 'block')};
  flex: ${props => (props.flex ? props.flex : null)};
  justify-content: ${props => (props.justifyContent ? props.justifyContent : null)};
  align-items: ${props => (props.alignItems ? props.alignItems : null)};
  width: ${props => props.width};
  font-weight: 600;
  font-size: ${props => (props.fontSize ? props.fontSize : '14px')};
  text-align: center;
  user-select: none;
  text-transform: uppercase;
  height: ${props => (props.height ? props.height : null)};
  position: relative;

  &::after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    height: 100%;
    width: 1px;
    background-color: #666;
  }
`;

export const HeaderList = styled.li`
  flex: 1;
  height: 40px;
  background: #d8d3d3;
  display: flex;
  margin-left: -40px;
  align-items: center;
`;

export const Content = styled.div`
  display: flex;
  background: rgba(8, 9, 9, 0.7);
  flex: 1;
  height: ${props => (props.height ? props.height : '388px')};
  margin-bottom: 30px;
  border: 5px solid #666;
  overflow: hidden;
  overflow-y: scroll;

  ::-webkit-scrollbar-track {
    background: #d7d7d7;
    border-radius: 10px;
  }

  ::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #707070;
    border-radius: 5px;

    :hover {
      background-color: #707070;
    }
  }
`;

export const Main = styled.div`
  flex: 1;
  position: relative;
  background: #e9f3e6;
  overflow: scroll;
  overflow-x: hidden;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: rgba(0, 0, 0, 0.5);
    box-shadow: 0 0 1px rgba(255, 255, 255, 0.5);
    -webkit-box-shadow: 0 0 1px rgba(255, 255, 255, 0.5);
  }
`;

export const MainContainer = styled.div`
  display: flex;
  width: 500px;
  flex-direction: column;
  position: relative;
  background: #333338;
  border-radius: 10px;
  padding: 40px;
  font-family: HiraginoSans-W6;

  @media only screen and (max-width: 480px) {
    width: 85vw;
    padding: 10vw 3vw;
  }
`;

export const Title = styled.div`
  width: 100%;
  font-size: 20px;
  letter-spacing: 3px;
  color: #ffffff;
  text-align: left;
  margin-bottom: 35px;
  line-height: 1;

  @media only screen and (max-width: 480px) {
    font-size: 5vw;
    text-align: center;
    margin-bottom: 6vw; 
  }
`;

export const UL = styled.ul`
  width: 100%;
  margin-top: 0;
  height: 0;
  left: 0;
  right: 0;
  margin-left: auto;
  margin-right: auto;
`;

export const ChipIcon = styled.img`
  width: 25px;
  height: 25px;
  margin-right: 5px;

  @media only screen and (max-width: 480px) {
    width: 5.5vw;
    height: 5.5vw;
  }
`;

export const TextItemContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${(props) => {
    if (props.alignLeft) return 'flex-start';
    if (props.alignRight) return 'flex-end';
    return 'center';
  }};
  padding: 5px;
  min-width: ${props => props.widthPC || '59.5%'};

  @media only screen and (max-width: 480px) {
    min-width: ${props => props.widthMB || '72%'};;
  }
`;

export const StyledTextHeaderList = styled(TextHeaderList)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => props.widthPC || '250px'};
  height: 100%;

  @media only screen and (orientation: portrait) and (max-width: 480px) {
    font-size: ${props => props.fontSizeMb || '4vw'};
    width: ${props => props.widthMB || '42vw'};
  }
`;

export const TableWrap = styled(Content)`
  margin: 0;
  overflow: hidden;
  overflow-x: hidden;
  overflow-y: auto;
  font-family: HiraginoSans-W6;
  li:nth-child(odd) {
    background-color: #4c4e4b;
  }
  li:nth-child(even) {
    background-color: #5c5a5a;
  }
`;

export const TableContainer = styled(Main)`
  overflow-y: auto;
  overflow-x: hidden;
`;

export const StyledLoadingAnimation = styled(LoadingAnimation)`
  top: 0%;
  left: 50%;
`;

export const StyledHeaderList = styled(HeaderList)`
  background: #d8d3d3 !important;
`;

export const StyledLineRight = styled(LineRight)`
  left: 206px;
  height: 200px;

  @media only screen and (max-width: 480px) {
    left: 43vw;
  }
`;
