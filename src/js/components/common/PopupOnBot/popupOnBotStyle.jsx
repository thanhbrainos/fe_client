import styled from 'styled-components';
import {
  Wrapper, Message,
} from '../Alert/alertStyle';
import checkedCheckBox from '../../../../assets/img/check_box_checked.png';
import uncheckCheckBox from '../../../../assets/img/check_box_uncheck.png';

export const ButtonConfirm = styled.div`
  display: flex;
`;

export const Checkbox = styled.div`
  background-image: url(${props => (props.isChecked ? checkedCheckBox : uncheckCheckBox)});
  background-size: 100% 100%;
  margin-right: 10px;
  width: 15px;
  height: 15px;
`;

export const WrapperOnBot = styled(Wrapper)`
  z-index: 1;
  overflow: auto;
`;

export const MessageContent = styled(Message)`
  font-size: 1.2em;
  margin-right: 1em;
  margin-left: ${props => props.marginLeft}em;
  width: ${props => (props.width ? props.width : 7)}em;
  text-align: ${props => (props.textAlign ? props.textAlign : 'end')};
`;

export const ContentItem = styled.div`
  color: #fff;
  display: flex;
  align-items: center;
  margin-top: ${props => (props.marginTop ? props.marginTop : 1.5)}em;
`;

export const ContentPopup = styled.div`
  color: #fff;
  font-size: ${props => props.fontSize}em;
  margin-bottom: 0.5em;
`;

export const ActionPopup = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: 1.5em;
`;


export const WrapperConfirm = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  .bet-note-gc {
    color: #ff0000;
    display: block;
    font-size: 0.95em;
    margin-bottom: -0.5em;
  }
`;

export const ContentConfirm = styled.div`
  width: 90%;
  background-color: rgb(255, 255, 255);
  border: 0.25em solid rgb(136, 136, 136);
  padding: 0.2em;
  font-size: 0.8em;
  margin-top: 1em;
`;

export const CheckboxConfirm = styled.div`
  display: flex;
  color: red;
  align-items: center;
  justify-content: center;
  font-weight: 700;
`;

export const MessageConfirm = styled.div`
  color: red;
  white-space: pre-wrap;
  text-align: center;
  font-size: 0.9em;
`;

export const filterListTable = (listTable) => {
  const listTableClone = JSON.parse(JSON.stringify(listTable));
  const result = [
    ...listTableClone.filter(item => item.status === 1),
    ...listTableClone.filter(item => item.status === 2),
    ...listTableClone.filter(item => item.status === 0),
  ];
  for (let i = 0; i < listTableClone.length; i += 1) {
    listTableClone[i].ignoreClick = (listTableClone[i].status !== 1);
  }
  return result;
};
