import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  color: ${props => props.color};
  width: auto;
  height: auto;
  display: flex;
  align-items: baseline;
`;

const WrapperInt = styled.div`
  font-size: 100%;
  width: auto;
  height: auto;
`;
const WrapperFloat = styled.div`
  font-size: 75%;
  width: auto;
  height: auto;
  display: flex;
  align-items: center;
`;

const padLeft = (number, length) => {
  const str = String(number);
  if (str.length >= length) return str;
  return number + Array(length - str.length + 1).join('0');
};

const conventNumberFloat = (num, afterDot) => {
  if (num === undefined) return false;
  const numToString = num.toString();
  const extract = numToString.split('.');
  let suffix = '';
  if (extract.length > 1) {
    suffix = extract[1];
  }
  suffix = suffix.substr(0, afterDot);
  suffix = padLeft(suffix, afterDot);
  return suffix;
};

const renderCaseNumberFloat = (value, afterDot) => {
  const numberInt = parseInt(value, 10);
  return (
    <React.Fragment>
      <WrapperInt>{numberInt.toLocaleString('ja')}</WrapperInt>.
      <WrapperFloat>{conventNumberFloat(value, afterDot)}</WrapperFloat>
    </React.Fragment>
  );
};

const renderCaseNumberInt = (value) => {
  const numberInt = parseInt(value, 10);
  return (
    <React.Fragment>
      <WrapperInt>{numberInt.toLocaleString('ja')}</WrapperInt>.<WrapperFloat><p>00</p></WrapperFloat>
    </React.Fragment>
  );
};

function StyleNumber(props) {
  const { value, color, afterDot } = props;
  if (value === null || value === undefined) {
    return (
      <Wrapper
        id="number"
        color={color}
      />
    );
  }
  return (
    <Wrapper
      id="number"
      color={color}
    >
      {
        Number.isInteger(value)
          ? renderCaseNumberInt(value)
          : renderCaseNumberFloat(value, afterDot)
      }
    </Wrapper>
  );
}

StyleNumber.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  afterDot: PropTypes.number,
  color: PropTypes.string,
};

StyleNumber.defaultProps = {
  afterDot: 2,
  color: '#fff',
  value: null,
};

export default StyleNumber;
