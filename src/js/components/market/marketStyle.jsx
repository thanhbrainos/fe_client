import React from 'react';
import styled from 'styled-components';
import {
  ButtonAddCampaign, IconAdd,
} from '../campaign/campaignStyle';
import images from '../../theme/images';
import i18n from '../../i18n/i18n';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: flex-start;
`;

export const COLORS_MARKET = {
  CARROT: '#ba610a',
  ORANGE: '#daa520',
  WHITE: '#fff5ec',
  GREEN: '#05ee23 ',
  RED: '#dc3545',
  DARK_BLUE: '#2d889c',
  NONE: '#00000000',
};

export const HeaderWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const ButtonAddGood = addGoodClick => (
  <ButtonAddCampaign
    onClick={addGoodClick}
  >
    <IconAdd src={images.add} alt="" />
    { i18n.t('market.addGood') }
  </ButtonAddCampaign>
);

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: ${props => props.top}em;
  width: 100%;
  flex-wrap: wrap;
`;

export const FILTER_BUY = {
  NONE: { id: 0, text: '' },
  NEW: { id: 1, text: 'New' },
  LIKE: { id: 2, text: 'Like' },
  PRICE: { id: 3, text: 'Price' },
  HOT: { id: 4, text: 'Hot' },
  OWN: { id: 5, text: 'Own' },
};
