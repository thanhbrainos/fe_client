import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { isMobile } from 'react-device-detect';
import {
  Wrapper, LeftWrapper, RightWrapper, NameText, NormalText,
  Blank, Row, GreenText,
} from './cellItemStyle';
import StyleNumber from '../../StyleNumber';
import i18n from '../../../i18n/i18n';
import { ButtonAction, Image } from '../../campaign/campaignStyle';
import { images } from '../../../theme';

const renderAction = (onClickDetail) => {
  return (
    <Fragment>
      <ButtonAction
        fontSize="1em"
        hoverBgColor="#20bcdf"
        padding={isMobile ? '0.5em' : '0.2em 0.5em 0.2em 0.5em'}
        opacity="0.5"
        margin="0 1em 0.7em 0"
        color="#2d889c"
        height={2}
        onClick={onClickDetail}
      >
        {isMobile || i18n.t('edit')}
        <Image src={images.edit2} alt="" />
      </ButtonAction>
    </Fragment>
  );
};

function CellItem(props) {
  return (
    <Wrapper>
      <LeftWrapper>
        <NameText>{props.name}</NameText>
        <Row>
          <NormalText>
            {i18n.t('price')}
            <Blank width={1}>:</Blank>
            <StyleNumber value={props.price} afterDot={2} />
          </NormalText>
          <Blank width={2} />
          <NormalText>
            {i18n.t('market.amount')}
            <Blank width={1}>:</Blank>
            {props.amount}
          </NormalText>
        </Row>
        <NormalText>
          {i18n.t('market.totalProfit')}
          <Blank width={1}>:</Blank>
          <GreenText>{props.profit}</GreenText>
          <Blank width={0.3} />{i18n.t('gc')}
        </NormalText>
      </LeftWrapper>
      <RightWrapper>
        { renderAction() }
      </RightWrapper>
    </Wrapper>
  );
}

CellItem.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  amount: PropTypes.number,
  profit: PropTypes.number,
};

CellItem.defaultProps = {
  name: '',
  price: 0,
  amount: 0,
  profit: 0,
};

export default CellItem;
