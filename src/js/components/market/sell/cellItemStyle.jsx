import styled from 'styled-components';
import { COLORS_MARKET } from '../marketStyle';
import { WrapperListItem } from '../../common/CommonStyle';

export const Wrapper = styled(WrapperListItem)`
  flex-direction: row;
`;

export const LeftWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  flex: 1;
  justify-content: flex-start;
`;

export const RightWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
`;

export const NameText = styled.div`
  color: ${COLORS_MARKET.ORANGE};
  font-size: 1.15em;
  font-weight: bold;
  align-text: left;
`;

export const NormalText = styled.div`
  color: ${COLORS_MARKET.WHITE};
  font-size: 1em;
  align-text: left;
  padding-left: ${props => props.left || 1}em;
  width: fit-content;
  display: flex;
  flex-direction: row;
`;

export const Blank = styled.div`
  width: ${props => props.width}em;
  height: ${props => props.height}em;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
`;

export const GreenText = styled.span`
  color: ${COLORS_MARKET.GREEN};
`;
