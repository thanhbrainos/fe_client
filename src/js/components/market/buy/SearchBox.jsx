import React from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper,
  InputSearch,
  ButtonSearch,
  IconSearch,
} from './searchBoxStyle';
import Images from '../../../../assets/images';
// import i18n from '../../../i18n/i18n';


function SearchBox(props) {
  const onClickSearch = () => {
    const input = document.getElementById('InputSearch');
    const { search } = props;
    if (input && search) {
      search(input.value);
    }
  };

  return (
    <Wrapper width={props.width} height={props.height} style={props.customStyle}>
      <InputSearch
        placeholder="Enter key search"
        maxLength={250}
        id="InputSearch"
      />
      <ButtonSearch width={props.height} onClick={onClickSearch}>
        <IconSearch src={Images.iconSearch} alt="search" />
      </ButtonSearch>
    </Wrapper>
  );
}

SearchBox.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  search: PropTypes.func,
  customStyle: PropTypes.objectOf(PropTypes.any),
};

SearchBox.defaultProps = {
  width: 22,
  height: 3,
  search: null,
  customStyle: {},
};

export default SearchBox;
