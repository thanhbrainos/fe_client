import styled from 'styled-components';
import { COLORS_MARKET } from '../marketStyle';
// import { WrapperListItem } from '../../common/CommonStyle';

export const CardWrapper = styled.div`
  width: 13em;
  height: 17.5em;
  background-color: #464646;
  border: 1px #464646 solid;
  display: flex;
  flex-direction: column;
  border-radius: 0.2em;
  margin: 0.5em;

  &: hover {
    box-shadow: 0.11em 0.28em 0.5em 0.056em rgba(0, 0, 0, 0.62);
    border: 1px white solid;
  }
`;

export const CardTitle = styled.div`
  width: 100%;
  padding-left: 0.5em;
  font-weight: bold;
  font-size: 1.1em;
  color: #ffffffab;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: ${props => props.top}em;
  width: 100%;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export const ColumnCenter = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-left: 0.5em;
  padding-right: 0.5em;
`;

export const IconLike = styled.img`
  width: 1.5em;
  margin-left: ${props => props.left}em;
`;

export const ColorText = styled.div`
  margin-left: ${props => props.left}em;
  margin-top: ${props => props.top}em;
  color: ${props => props.color};
  font-weight: bold;
`;

export const ButtonBuy = styled.div`
  width: ${props => props.width}em;
  height: ${props => props.height}em;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.active ? COLORS_MARKET.CARROT : COLORS_MARKET.NONE)};
  border: 1px ${COLORS_MARKET.CARROT} solid;
  border-radius: 0.2em;
  font-weight: bolder;
  margin-top: ${props => props.top}em;
  margin-left: ${props => props.left}em;

  &: hover {
    background-color: ${COLORS_MARKET.ORANGE};
  }
`;
