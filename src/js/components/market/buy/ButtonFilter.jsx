import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { COLORS_MARKET } from '../marketStyle';

const Wrapper = styled.div`
  width: ${props => props.width}em;
  height: ${props => props.height}em;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${props => (props.active ? COLORS_MARKET.CARROT : COLORS_MARKET.NONE)};
  border: 1px ${COLORS_MARKET.CARROT} solid;
  border-radius: 0.2em;
  font-weight: bolder;
  margin-top: ${props => props.top}em;
  margin-left: ${props => props.left}em;

  &: hover {
    background-color: ${COLORS_MARKET.ORANGE};
  }
`;

function ButtonFilter(props) {
  return (
    <Wrapper
      width={props.width}
      height={props.height}
      active={props.active}
      top={props.top}
      left={props.left}
      onClick={props.onClick}
    >
      {props.text}
    </Wrapper>
  );
}

ButtonFilter.propTypes = {
  text: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  top: PropTypes.number,
  left: PropTypes.number,
  active: PropTypes.bool,
  onClick: PropTypes.func,
};

ButtonFilter.defaultProps = {
  width: 7.5,
  height: 3,
  active: false,
  top: 0,
  left: 0,
  onClick: null,
};

export default ButtonFilter;
