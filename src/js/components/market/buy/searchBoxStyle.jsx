import styled from 'styled-components';
import { COLORS_MARKET } from '../marketStyle';

export const Wrapper = styled.div`
  width: ${props => props.width}em;
  height: ${props => props.height}em;
  display: flex;
  flex-direction: row;
`;

export const InputSearch = styled.input`
  height: 100%;
  background-color: #d6d6d6;
  flex: 1;
  outline: none;
  border: none;
  padding-left: 0.5em;
  font-size: 1.3em;
  color: #dc720a;
  font-weight: bold;
  border-radius: 0.1em 0 0 0.1em;

  ::-webkit-input-placeholder {
    color: #dc720a7a;
  }
`;

export const ButtonSearch = styled.div`
  width: ${props => props.width}em;
  height: 100%;
  background-color: ${COLORS_MARKET.CARROT};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-radius: 0 0.1em 0.1em 0;

  &: hover {
    background-color: #f26522;
  }
`;


export const IconSearch = styled.img`
  width: 40%;
`;
