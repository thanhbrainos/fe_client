import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { isMobile } from 'react-device-detect';
import {
  CardWrapper, CardTitle, Column, Row, IconLike,
  ColorText, ButtonBuy, ColumnCenter,
} from './buyItemStyle';
import images from '../../../../assets/images';
import i18n from '../../../i18n/i18n';
import { COLORS_MARKET } from '../marketStyle';


function BuyItem(props) {
  return (
    <CardWrapper>
      <CardTitle>{props.name}</CardTitle>
      <img width="100%" src={images.marketDefaultItemAvatar} alt="avatar" />
      <Row>
        <Column>
          <Row top={0.5}>
            <IconLike src={images.iconLike} alt="" left={0.3} />
            <ColorText color="#f9ab05" left={0.3}>{props.like.toLocaleString('ja')}</ColorText>
          </Row>
          <ColorText color={COLORS_MARKET.CARROT} left={0.3} top={0.3}>
            {props.price.toLocaleString('ja')} {i18n.t('gc')}
          </ColorText>
        </Column>
        <ColumnCenter>
          <ButtonBuy
            width={3.2}
            height={1.5}
            active={props.canBuy}
          >Buy
          </ButtonBuy>
        </ColumnCenter>
      </Row>
    </CardWrapper>
  );
}

BuyItem.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  like: PropTypes.number,
  canBuy: PropTypes.bool,
  avatar: PropTypes.string,
};

BuyItem.defaultProps = {
  name: 'Super Win',
  price: 1500,
  like: 1000,
  canBuy: true,
  avatar: '',

};

export default BuyItem;
