import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper, ButtonAddGood, HeaderWrapper,
} from './marketStyle';
import CellItem from './sell/CellItem';

class CellMarketItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCellItem: [
        {
          id: 1,
          name: 'Bet logic 1',
          price: 120,
          amount: 432,
          profit: 51840,
        },
        {
          id: 2,
          name: 'Bet logic 2',
          price: 120,
          amount: 432,
          profit: 51840,
        },
        {
          id: 3,
          name: 'Bet logic 3',
          price: 120,
          amount: 432,
          profit: 51840,
        },
      ],
    };
  }

  componentDidMount() {
    this.setHeader();
  }

  onClickNotifyButton() {
    const { isShowAll } = this.state;
    this.setState({ isShowAll: !isShowAll });
  }

  setHeader(checkVisible = true) {
    const { updateHeaderTitle, visible } = this.props;
    if (checkVisible && !visible) return;
    updateHeaderTitle(
      <HeaderWrapper>
        Total: 3
        <ButtonAddGood />
      </HeaderWrapper>,
    );
  }

  UNSAFE_componentWillReceiveProps(newProps) {
    const { visible } = this.props;
    if (visible !== newProps.visible && newProps.visible === true) {
      this.setHeader(false);
    }
  }

  render() {
    const { visible } = this.props;
    const { listCellItem } = this.state;
    if (!visible) return null;
    return (
      <Wrapper>
        {
          listCellItem.map(item => (
            <CellItem
              key={item.id}
              name={item.name}
              price={item.price}
              amount={item.amount}
              profit={item.profit}
            />
          ))
        }
      </Wrapper>
    );
  }
}

CellMarketItems.propTypes = {
  visible: PropTypes.bool,
  updateHeaderTitle: PropTypes.func,
};

CellMarketItems.defaultProps = {
  visible: true,
  updateHeaderTitle: null,

};

export default CellMarketItems;
