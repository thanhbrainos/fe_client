import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper, Row, FILTER_BUY,
} from './marketStyle';
import SearchBox from './buy/SearchBox';
import ButtonFilter from './buy/ButtonFilter';
import i18n from '../../i18n/i18n';
import BuyItem from './buy/BuyItem';


class BuyMarketItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKey: '',
      filter: FILTER_BUY.NONE.id,
    };
  }

  onClickFilter(filterId) {
    const { filter } = this.state;
    if (filter === filterId) {
      this.setState({ filter: FILTER_BUY.NONE.id });
    } else {
      this.setState({ filter: filterId });
    }
  }

  render() {
    const { visible } = this.props;
    const { filter } = this.state;
    const copyFilters = [FILTER_BUY.NEW, FILTER_BUY.LIKE,
      FILTER_BUY.PRICE, FILTER_BUY.HOT, FILTER_BUY.OWN];

    const filterButtons = copyFilters.map(item => (
      <ButtonFilter
        text={i18n.t(item.text)}
        left={0.5}
        top={0.5}
        active={filter === item.id}
        onClick={() => this.onClickFilter(item.id)}
        key={item.id}
      />
    ));

    if (!visible) return null;
    return (
      <Wrapper>
        <Row>
          <SearchBox customStyle={{ marginTop: '0.5em' }} />
          {filterButtons}
        </Row>
        <Row top={0.5}>
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
          <BuyItem />
        </Row>
      </Wrapper>
    );
  }
}

BuyMarketItems.propTypes = {
  visible: PropTypes.bool,
  updateHeaderTitle: PropTypes.func,
};

BuyMarketItems.defaultProps = {
  visible: true,
  updateHeaderTitle: null,
};

export default BuyMarketItems;
