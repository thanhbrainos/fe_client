import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper,
} from './marketStyle';


class HistoryMarket extends Component {
  onClickNotifyButton() {
    const { isShowAll } = this.state;
    this.setState({ isShowAll: !isShowAll });
  }

  render() {
    const { visible } = this.props;
    if (!visible) return null;
    return (
      <Wrapper>
        History
      </Wrapper>
    );
  }
}

HistoryMarket.propTypes = {
  visible: PropTypes.bool,
  updateHeaderTitle: PropTypes.func,
};

HistoryMarket.defaultProps = {
  visible: true,
  updateHeaderTitle: null,
};

export default HistoryMarket;
