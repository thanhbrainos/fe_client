import React, { Component, Fragment } from 'react';
import PropsType from 'prop-types';
import Alert from '../common/Alert/Alert';
import ApiErrorUtils from '../../helpers/ApiErrorUtils';
import images from '../../theme/images';
import i18n from '../../i18n/i18n';
import InputText from '../common/InputText';
import Dropdown from '../common/Dropdown/Dropdown';
import Spinner from '../common/Spinner';
import {
  ModalFooter, ModalBodyCustom, ModalCustom,
  ModalHeaderCustom, Hr,
  BodyContentPopup, ButtonDisable,
} from '../mainContainer/mainStyle';
import { Button } from './listBotsStyle';
import { convertCampaignOption } from '../../helpers/utils';

const onSuccessFetchListCampaign = (data) => {
  ApiErrorUtils.handleServerError(data, Alert.instance, () => { });
};

class AddBot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      botInfo: {
        name: '',
        campaign_id: '',
        isError: false,
        errorMessage: '',
      },
      isShowPopup: false,
      isLoading: false,
    };

    this.closeModal = this.closeModal.bind(this);
    this.handleChangeBotName = this.handleChangeBotName.bind(this);
    this.handleChangeCampaign = this.handleChangeCampaign.bind(this);
    this.onSuccessAddBots = this.onSuccessAddBots.bind(this);
    this.onError = this.onError.bind(this);
  }

  componentDidMount() {
    this.fetchListCampaigns();
  }

  onSuccessAddBots(data) {
    ApiErrorUtils.handleServerError(data, Alert.instance, () => {
      setTimeout(() => {
        this.closeModal();
        this.setState({ isLoading: false });
        this.props.fetchListBots();
      }, 500);
    }, () => this.setState({ isLoading: false }));
  }

  onError(error) {
    try {
      ApiErrorUtils.handleHttpError(error, Alert.instance);
    } catch (err) {
      // do something
    } finally {
      this.setState({ isLoading: false });
    }
  }

  onClickAddBot() {
    const { createBots } = this.props;
    const { botInfo } = this.state;
    if (this.validateBotInfos()) {
      const params = [botInfo];
      this.setState({ isLoading: true });
      createBots(this.onSuccessAddBots, this.onError, params);
    }
  }

  closeModal() {
    this.setState({
      isShowPopup: false,
      botInfo: {
        name: '',
        campaign_id: '',
        isError: false,
        isErrorMessage: '',
      },
    });
  }

  showModal() {
    const { isAddBot, maxBot } = this.props;
    const alert = Alert.instance;
    if (!isAddBot) {
      alert.showAlert(
        i18n.t('error'),
        i18n.t('botLimited', { maxBot }),
        i18n.t('OK'),
        () => alert.hideAlert(),
      );
    } else {
      this.fetchListCampaigns();
      this.setState({
        isShowPopup: true,
      });
    }
  }

  handleChangeBotName(value) {
    const { botInfo } = this.state;
    botInfo.name = value;
    botInfo.isError = !value;
    botInfo.errorMessage = value ? '' : i18n.t('txtFieldMinSize');
    this.setState({ botInfo });
  }

  handleChangeCampaign(infoId, infos) {
    const { botInfo } = this.state;
    botInfo.campaign_id = infos.value;
    this.setState({ botInfo });
  }

  validateBotInfos() {
    const { botInfo } = this.state;
    let isValidate = true;
    if (!botInfo.name || botInfo.isError) {
      botInfo.errorMessage = botInfo.isError ? botInfo.errorMessage : i18n.t('txtFieldMinSize');
      botInfo.isError = true;
      isValidate = false;
    }
    this.setState({ botInfo });
    return isValidate;
  }

  fetchListCampaigns() {
    const { fetchListCampaigns } = this.props;
    fetchListCampaigns(onSuccessFetchListCampaign, this.onError, null);
  }

  renderBodyAddBot(item) {
    const { listCampaigns } = this.props;
    const { botInfo } = this.state;
    const campaignData = convertCampaignOption(listCampaigns, 342);
    return (
      <BodyContentPopup key={0}>
        <InputText
          title={i18n.t('botName')}
          value={item.bot_name}
          index={0}
          isError={botInfo.isError}
          errorMessage={botInfo.errorMessage}
          size={20}
          minSize={1}
          height="2em"
          width={campaignData.maxLength}
          handleChangeInput={this.handleChangeBotName}
          isFlexColumn
        />
        <Dropdown
          index={0}
          data={campaignData.campaignOption}
          title={i18n.t('campaign')}
          heightOptions={10}
          width={campaignData.maxLength}
          onChangeSelected={this.handleChangeCampaign}
          isFlexColumn
        />
        <Hr />
      </BodyContentPopup>
    );
  }

  renderPopupAddBot() {
    const { isShowPopup, botInfo } = this.state;
    const { fontSize } = this.props;
    return (
      <ModalCustom
        centered
        isOpen={isShowPopup}
        fontSize={fontSize * 0.9}
      >
        <ModalHeaderCustom toggle={this.closeModal}>{i18n.t('addBot')}</ModalHeaderCustom>
        <ModalBodyCustom>
          {
            this.renderBodyAddBot(botInfo)
          }
          <ModalFooter>
            <ButtonDisable
              hoverBgColor="#23B083"
              width="9em"
              height="2.75em"
              color="#2d889c"
              onClick={() => this.onClickAddBot()}
            >
              {i18n.t('addBot')}
            </ButtonDisable>
          </ModalFooter>
        </ModalBodyCustom>
      </ModalCustom>
    );
  }

  render() {
    const { isShowPopup } = this.state;
    return (
      <Fragment>
        <Button
          padding="1em"
          hoverBgColor="#20bcdf"
          onClick={() => this.showModal()}
        >
          <React.Fragment>
            <img
              src={images.add}
              alt="view mode"
              id="view-mode-btn"
            />
            {i18n.t('addBot')}
          </React.Fragment>
        </Button>
        {isShowPopup && this.renderPopupAddBot()}
        <Spinner isLoading={this.state.isLoading} />
      </Fragment>
    );
  }
}

AddBot.propTypes = {
  createBots: PropsType.func.isRequired,
  fontSize: PropsType.number.isRequired,
  fetchListBots: PropsType.func.isRequired,
  fetchListCampaigns: PropsType.func.isRequired,
  listCampaigns: PropsType.any.isRequired,
  isAddBot: PropsType.bool.isRequired,
  maxBot: PropsType.number.isRequired,
};
export default AddBot;
