import React, { Component } from 'react';
import PropsType from 'prop-types';
import Pagination from 'rc-pagination';
import { isMobile } from 'react-device-detect';
import {
  WrapperTable, Row, Cell,
  RowHead, Table, CellBreak,
} from './ChargeStyle';
import { WrapperPaginationCustom, fontSize, SpanCharge } from '../common/CommonStyle';
import i18n from '../../i18n/i18n';
import StyleNumber from '../StyleNumber';
import { Image } from '../listBots/listBotsStyle';
import lucImage from '../../../assets/lucImage';

const PER_PAGE = 10;

const renderPayoutDetail = (data) => {
  if (!data) return 'FGC';
  const keys = Object.keys(data);
  const result = [];
  for (let i = 0; i < keys.length; i += 1) {
    if (data[keys[i]] !== 0) {
      result.push(
        <>
          {keys[i]}&nbsp;(<StyleNumber value={data[keys[i]]} color="#fff" afterDot={2} />)&nbsp;&nbsp;
        </>,
      );
    }
  }
  return result;
};

const renderHeader = () => (
  <RowHead>
    <Cell>{i18n.t('date')}</Cell>
    <Cell>{i18n.t('botName')}</Cell>
    <Cell>{i18n.t('type')}</Cell>
    <Cell>{i18n.t('payoutTotal')}</Cell>
    <Cell>{i18n.t('payoutDetail')}</Cell>
    <Cell>{i18n.t('after')}</Cell>
  </RowHead>
);

class HistoryPayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
    };

    this.fetchHistoryPayout = this.fetchHistoryPayout.bind(this);
    this.changePage = this.changePage.bind(this);
  }

  componentDidMount() {
    this.fetchHistoryPayout();
  }

  fetchHistoryPayout() {
    const { currentPage } = this.state;
    this.props.fetchHistoryPayout(PER_PAGE, currentPage);
  }

  changePage(page) {
    this.setState({
      currentPage: page,
    }, () => { this.fetchHistoryPayout(); });
  }

  renderContent() {
    const { historyPayout } = this.props;
    return (
      Object.keys(historyPayout).length > 0
      && (
        <React.Fragment>
          {
            historyPayout.data.map((item, id) => {
              const itemName = item.is_deleted ? item.bot_id + i18n.t('botNameDeleted') : item.name;
              return (
                <Row backgroundColor={id % 2 === 0 ? '#555' : '#333'} key={item.id}>
                  <Cell textAlign="left"><CellBreak width={6.5} isMobile={isMobile}>{item.date}</CellBreak></Cell>
                  <Cell textAlign="left"><CellBreak width={10} isMobile={isMobile}>{itemName}</CellBreak></Cell>
                  <Cell textAlign="left">{i18n.t('payoutManually')}</Cell>
                  <Cell textAlign="right">
                    <SpanCharge style={{ alignItems: 'center' }}>
                      <Image
                        src={lucImage.coins.FGCchip}
                        style={{
                          width: '1.5em',
                          marginBottom: 0,
                          height: '1.5em',
                          marginRight: '0.2em',
                        }}
                      />
                      <StyleNumber value={item.payout} color="#fff" afterDot={2} />
                    </SpanCharge>
                  </Cell>
                  <Cell textAlign="left"><SpanCharge style={{ whiteSpace: 'pre' }}>{renderPayoutDetail(typeof (item.gc_detail) === 'string' ? JSON.parse(item.gc_detail) : item.gc_detail)}</SpanCharge></Cell>
                  <Cell textAlign="right">
                    <SpanCharge style={{ alignItems: 'center' }}>
                      <Image
                        src={lucImage.coins.FGCchip}
                        style={{
                          width: '1.5em',
                          marginBottom: 0,
                          height: '1.5em',
                          marginRight: '0.2em',
                        }}
                      />
                      <StyleNumber value={item.gc_main_account} color="#fff" afterDot={2} />
                    </SpanCharge>
                  </Cell>
                </Row>
              );
            })
          }
        </React.Fragment>
      )
    );
  }

  render() {
    const { currentPage } = this.state;
    const { historyPayout } = this.props;
    const total = historyPayout.total ? Math.ceil(historyPayout.total / PER_PAGE) : 1;
    return (
      <React.Fragment>
        <WrapperTable>
          <Table isMobile={isMobile}>
            <tbody>
              {renderHeader()}
              {this.renderContent()}
            </tbody>
          </Table>
        </WrapperTable>
        <WrapperPaginationCustom height={3} width={40} scale={fontSize / 18}>
          {total > 1
            && (
              <Pagination
                current={currentPage}
                pageSize={PER_PAGE}
                total={historyPayout.total}
                onChange={(page) => { this.changePage(page); }}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              />
            )}
        </WrapperPaginationCustom>
      </React.Fragment>
    );
  }
}

HistoryPayout.defaultProps = {
  historyPayout: {},
};

HistoryPayout.propTypes = {
  historyPayout: PropsType.object,
  fetchHistoryPayout: PropsType.func.isRequired,
};

export default HistoryPayout;
