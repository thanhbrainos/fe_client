import i18n from '../../i18n/i18n';

export const MARKET_TABS = {
  BUY: { id: 1, text: i18n.t('market.buy') },
  CELL: { id: 2, text: i18n.t('market.cell') },
  HISTORY: { id: 3, text: i18n.t('market.history') },
};

export const listTabs = Object.values(MARKET_TABS);
