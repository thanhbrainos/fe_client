import React, { Component } from 'react';
import 'rc-pagination/assets/index.css';
// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ApiErrorUtils from '../../helpers/ApiErrorUtils';
import Spinner from '../../components/common/Spinner';
import Alert from '../../components/common/Alert/Alert';
// import i18n from '../../i18n/i18n';
import {
  ContentContainer, ContentBody, ContentHeader, MedianStrip,
} from '../../components/common/CommonStyle';
import { MARKET_TABS, listTabs } from './marketStyle';
import TabMenu from '../../components/campaign/detail/TabMenu';
import BuyMarketItems from '../../components/market/BuyMarketItems';
import CellMarketItems from '../../components/market/CellMarketItems';
import HistoryMarket from '../../components/market/HistoryMarket';

class Market extends Component {
  constructor(props) {
    super(props);

    this.state = {
      headerTitle: 'title',
      currentTabId: listTabs[0].id,
    };
    this.onSuccess = this.onSuccess.bind(this);
    this.onError = this.onError.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.updateHeaderTitle = this.updateHeaderTitle.bind(this);
  }

  componentDidMount() {
  }

  onSuccess(data) {
    ApiErrorUtils.handleServerError(data, Alert.instance, () => {
      this.setState({ isLoading: false });
    });
  }

  onError(data) {
    ApiErrorUtils.handleHttpError(data, Alert.instance, () => {
    });
    this.setState({ isLoading: false });
  }

  changeTab(tabId) {
    this.setState({ currentTabId: tabId });
  }

  updateHeaderTitle(title) {
    this.setState({ headerTitle: title });
  }

  render() {
    // const { data } = this.props;
    const { headerTitle, currentTabId } = this.state;
    // const { total, listNews } = data;
    return (
      <ContentContainer>
        <ContentHeader>
          { headerTitle }
        </ContentHeader>
        <MedianStrip />
        <ContentBody>
          <TabMenu
            tabs={listTabs}
            selectTedId={currentTabId}
            onChangeTab={this.changeTab}
          />
          <BuyMarketItems
            visible={currentTabId === MARKET_TABS.BUY.id}
            updateHeaderTitle={this.updateHeaderTitle}
          />
          <CellMarketItems
            visible={currentTabId === MARKET_TABS.CELL.id}
            updateHeaderTitle={this.updateHeaderTitle}
          />
          <HistoryMarket
            visible={currentTabId === MARKET_TABS.HISTORY.id}
            updateHeaderTitle={this.updateHeaderTitle}
          />
        </ContentBody>
        <Spinner isLoading={this.state.isLoading} />
      </ContentContainer>
    );
  }
}

Market.propTypes = {
  actions: PropTypes.object.isRequired,
  fontSize: PropTypes.number.isRequired,
  portraitMode: PropTypes.bool.isRequired,
  data: PropTypes.object.isRequired,
};

// function mapStateToProps(state) {
//   return {
//     data: {
//       listNews: state.news.listNews,
//       newDetail: state.news.newDetail,
//       total: state.news.total,
//     },
//   };
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     actions: {
//       fetchListNews: bindActionCreators(newsActions.fetchListNews, dispatch),
//       fetchNewDetail: bindActionCreators(newsActions.fetchNewDetail, dispatch),
//       updateHasReadNew: bindActionCreators(newsActions.updateHasReadNew, dispatch),
//     },
//   };
// }

export default Market;
